﻿(function ($) {

    "use strict";

    /* ==========================================================================
                            check document is ready, then
   ========================================================================== */

    $(document).ready(function () {
        
            var $boyCounter = $(".boycounter");

            if ($boyCounter.length) {

                $.backstretch([
                             '/resource/image/m2.JPG'
                             , '/resource/image/m1.JPG'
				],
                         { duration: 4000, fade: 1000 })
                ;

                $boyCounter.tictic({
					totalWeeks: 8,
                    date: {
                        year: 2015,
                        month: 3,
                        day: 10
                    },
                    charts: {
                        disableAnimation: false
                    }
                });

            }
    });

})(window.jQuery);

